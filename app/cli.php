<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/9/19 0019
 * Time: 11:51
 */
namespace App;
/**
 * 队列实现
 */

use Phalcon\DI\FactoryDefault\CLI as CliDI;
use Phalcon\CLI\Console as ConsoleApp;
$di = new CliDI();
defined('APPLICATION_PATH')|| define('APPLICATION_PATH', realpath(dirname(__FILE__)));

$loader = new \Phalcon\Loader();

$loader->registerDirs(array(APPLICATION_PATH . '/Task'));
$loader->register();

$console = new ConsoleApp();

//di操作

$console->setDI($di);

/*** Process the console arguments*/
$arguments = array();
$params = array();

foreach($argv as $k => $arg) {
    if($k == 1) {
        $arguments['task'] = $arg;
    } elseif($k == 2) {
        $arguments['action'] = $arg;
    } elseif($k >= 3) {
        $params[] = $arg;
    }
}
if(count($params) > 0) {
    $arguments['params'] = $params;
}
// define global constants for the current task and action
define('CURRENT_TASK', (isset($argv[1]) ? $argv[1] : null));
define('CURRENT_ACTION', (isset($argv[2]) ? $argv[2] : null));
try {
    print_r($arguments);
    // handle incoming arguments
    $console->handle($arguments);
}catch (\Phalcon\Exception $e) {
    echo $e->getMessage();
    exit(255);
}

