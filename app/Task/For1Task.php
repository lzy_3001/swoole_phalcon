<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/8/10 0010
 * Time: 11:59
 */
namespace App\Task;
use App\Task\Contracts\ITask;

class For1Task implements ITask
{

    /**
     * @param $server
     * @param $task_id task_id会一直变化但在同一个任务中中是不变的
     * @param $reactor_id 来自哪一个reactor 一般用来做统计
     * @param $params 这里是要传递过来的业务参数
     * @return mixed
     */
    public function run($server, $task_id, $reactor_id, $params)
    {
        echo __METHOD__, PHP_EOL;
    }
}