<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/9/19 0019
 * Time: 11:53
 */



class ConsumeTask extends \Phalcon\CLI\Task{

    public function mainAction()    {
        //连接Beanstalk
        $queue = new Phalcon\Queue\Beanstalk([
            'host' => '127.0.0.1',
            'port' => 11301
        ]);
        //监视指定tube
        $queue->watch("my_tube");

        while(true){
            echo 'Waiting for a job... STRG+C to abort.'."\n";
            //获取任务
            $job = $queue->reserve();
            if(!$job){
                echo 'Invalid job found. Not processing.'."\n";
            }else{
                $job_id = $job->getId();
                echo 'Processing job '.$job_id."\n";
                //获取任务详情
                $jobInfo = $job->getBody();
                echo 'Msg:'.$jobInfo['msg']."\n";
                $job->delete();
                echo 'Success Job '.$job_id.'. Deleting.'."\n";
            }
        }
    }

}

