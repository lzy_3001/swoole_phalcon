<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/9/19 0019
 * Time: 11:52
 */



class ProductionTask extends \Phalcon\CLI\Task{

    public function mainAction()    {
        //连接Beanstalk
        $queue = new Phalcon\Queue\Beanstalk([
            'host' => '127.0.0.1',
            'port' => 11301
        ]);
        //choose方法指定tube
        $queue->choose("my_tube");
        //创建任务
        for($i=0;$i<10;$i++){
            $queueId = $queue->put(['msg' => 'hello phalcon('.$i.')']);
            echo '任务Id:'.$queueId."\n";
        }
    }

}
