<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/8/10 0010
 * Time: 19:10
 */
namespace App\Base\Model;
use Phalcon\Mvc\Model;

class BaseModel extends Model{
    protected $table = '';
    public function initialize(){
        //映射数据表（补上表前缀）
        $this->set_table_source($this->table);
    }
    /**
     * 映射数据表（补上表前缀）
     * @param string $tableName
     * @param null $prefix
     */
    protected function set_table_source($tableName=null, $prefix = null){
        if(!empty($tableName)){
            //默认从配置中读取表前缀配置
            if(empty($prefix)){
                $dbCfg = include DOCUMENT_ROOT."config/db.php";
                $prefix= $dbCfg->db["prefix"];
            };
            //拼接成完整表名之后，再通过setSource()映射数据表
            $this->setSource($prefix . $tableName);
        }else{
            $tableName=$this->getSource();
            if(empty($prefix)){
                $dbCfg = include DOCUMENT_ROOT."config/db.php";
                $prefix= $dbCfg->db["prefix"];
            };
            $this->setSource($prefix . $tableName);
        }
    }
}