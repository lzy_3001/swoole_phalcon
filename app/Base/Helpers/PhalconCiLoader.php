<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/8/10 0010
 * Time: 14:42
 */
namespace App\Base\Helpers;

use Phalcon\Db\Adapter\MongoDB\Client;
use Phalcon\Di\FactoryDefault;
use Phalcon\Mvc\Router;
use Phalcon\Mvc\View;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Redis as NaRedis;
use Phalcon\Cache\Backend\Redis;
use Phalcon\Cache\Frontend\Data;
class PhalconCiLoader{
    /**
     * 装载ci文件
     * @return FactoryDefault
     */
    public static function loadDiConfig(){
        $di = new FactoryDefault();

        //加载数据库
        $dbCfg = include APP_ROOT."config/db.php";
        $dbAdapter='Phalcon\Db\Adapter\Pdo\\'.$dbCfg->db['adapter'];
        $di->set(
            "db",
            function () use ($dbAdapter,$dbCfg){
                return new $dbAdapter(
                    [
                        "host"     => $dbCfg->db["host"],
                        "username" => $dbCfg->db["username"],
                        "password" => $dbCfg->db["password"],
                        "dbname"   => $dbCfg->db["dbname"],
                        'charset'	  => $dbCfg->db["charset"],
                       // 'prefix'	  => $dbCfg->db["prefix"],  //无法设置表的前缀可用继承
                    ]
                );
            }
        );
//        $dbconfig=APP_ROOT."config/db.php";
//        $di->set("dbCfg",require $dbconfig,true);
//        $di->set(
//            "db",
//            function () {
//                return new DbAdapter(
//                    [
//                        "host"     => "127.0.0.1",
//                        "username" => "root",
//                        "password" => "mima_8080",
//                        "dbname"   => "phalcon",
//                        'charset'	  => 'utf8',
//                    ]
//                );
//            }
//        );
        /**
         * 加载路由
         */
        $di->set('router', function () {
            $router = new Router();
            //首页路由
            $router->add('/', [
                'namespace' => 'App\Base\Controller',
                'controller' => 'Index',
                'action' => 'index'
            ]);
            //定义路由 /模块/控制器/方法
            $router->add('/:module/:controller/:action', [
                'module' => 1,
                'controller' => 2,
                'action' => 3
            ]);
            /**
             * 加载路由
             */
            include_once APP_ROOT . '/router/all.php';
            return $router;
        });
        /**
         * 请求路径
         */
        $di->set("url",function(){
            $url = new \Phalcon\Mvc\Url();
            $url->setBaseUri('/');
            return $url;
        });
        /**
         * 注册视图
         */
        $di->set('view', function () {
            $view = new View();
            $view->disable();
            return $view;
        }, true);

        /***
         * mongdb连接
         */
        $di->setShared('mongo', function () use ($di) {
            $configs = $di->getShared('config');
            $config = $configs['database']['mongo'];
            if(!empty($config['username']) || !empty($config['password'])){
                $mongo = new Client('mongodb://'.$config['username'].':'.$config['password'].'@'.$config['host'].':'.$config['port']);
            }else{
                $mongo = new Client('mongodb://'.$config['host'].':'.$config['port']);
            }
            return $mongo->selectDatabase($config["dbname"]);
        });
        /**
         * redis
         */
        $di->setShared('redis', function () use ($di) {
            $config = $di->getShared('config');
            $config = $config['database']['redis'];
            $frontCache = new Data (
                [
                    "lifetime" => 172800, //2day
                ]
            );
            $redis = new Redis($frontCache, [
                'prefix' => '5v5:',
                'lifetime' => 86400,
                'host' => $config['host'],
                'port' => $config['port'],
                "auth" => $config['auth'],
                'persistent' => false,
                'index' => 0
            ]);
            return $redis;
        });
        /***
         * redis 原生联接
         */
        $di->setShared('native_redis', function () use ($di) {
            $config = $di->getShared('config');
            $config = $config['database']['redis'];
            $redis = new NaRedis();
            $redis->pconnect($config['host'], $config['port']);
            $redis->auth($config['auth']);
            return $redis;
        });

        return $di;
    }


}