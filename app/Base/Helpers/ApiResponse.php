<?php
// +----------------------------------------------------------------------
// | 竞技荣耀科技项目
// +----------------------------------------------------------------------
// | Copyright (c) 2017.10.24 All rights reserved.
// +----------------------------------------------------------------------
// | Author: 林子彦 <278805354@qq.com>
// +----------------------------------------------------------------------
namespace App\Base\Helpers;
trait ApiResponse{
    //响应工具
    use ResponseUtil;
    //响应正确状态码
    protected $statusCode = 200;
    //响应错误状态码
    protected $statusErrorCode=500;
    /**
     * 获取状态码
     *
     * @return string|unknown
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * 设置状态码
     *
     * @param
     *            $statusCode
     * @return $this
     */
    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        return $this;
    }

    /**
     * 获取状态码
     *
     * @return string|unknown
     */
    public function getStatusErrorCode()
    {
        return $this->statusErrorCode;
    }

    /**
     * 设置状态码
     *
     * @param
     *            $statusCode
     * @return $this
     */
    public function setStatusErrorCode($statusCode)
    {
        $this->statusErrorCode = $statusCode;
        return $this;
    }

    /**
     * 成功返回
     * @param string $message
     * @param array $datas
     * @param bool $ajax
     */
    public function success($message='',$datas='',$ajax=true){
        return $this->dispatchJump($message,$this->statusCode,$datas,$ajax);
    }

    /**
     * 失败返回
     * @param string $message
     * @param array $datas
     * @param bool $ajax
     */
    public function error($message='',$datas='',$ajax=true){
        $this->dispatchJump($message,$this->statusErrorCode,$datas,$ajax);
    }


}
?>