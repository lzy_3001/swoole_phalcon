<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/8/13 0013
 * Time: 11:34
 */
namespace App\Base\Helpers;
use App\Base\Exception\ApiException;

trait ResponseUtil{
    use CommonFunction;

    /**
     * 操作
     * @param $message
     * @param int $status
     * @param string $jumpUrl
     * @param bool $ajax
     */
    public function dispatchJump($message,$status=1,$datas='',$ajax=false){

        if(true === $ajax || IS_AJAX) {// AJAX提交

            $data           =   is_array($ajax)?$ajax:array();
            $data['code'] =   $status;
            $data['message']   =   $message;
            $data['data']    =   $datas;
           return  $this->ajaxReturn($data);
        }
        //当返回不是Json格式，则生成返回输出

    }
    /**
     * Ajax方式返回数据到客户端
     * @access protected
     * @param mixed $data 要返回的数据
     * @param String $type AJAX返回数据格式zsd
     * @param int $json_option 传递给json_encode的option参数
     * @return void
     */
    protected function ajaxReturn($data,$type='',$json_option=0) {
        if(empty($type)) $type  =   "JSON";
        switch (strtoupper($type)){
            case 'JSON' :
//                $response=\Phalcon\Http\Response();
//                // 返回JSON数据格式到客户端 包含状态信息
//                $response->header('Content-Type:application/json; charset=utf-8');
                return  json_encode($data,$json_option);
            case 'XML'  :
                // 返回xml格式数据
                header('Content-Type:text/xml; charset=utf-8');
                exit($this->xml_encode($data));
            case 'JSONP':
                // 返回JSON数据格式到客户端 包含状态信息
                header('Content-Type:application/json; charset=utf-8');
                $handler  =   isset($_GET['callback']) ? $_GET['callback'] :'jsonpReturn';
                exit($handler.'('.json_encode($data,$json_option).');');
            case 'EVAL' :
                // 返回可执行的js脚本
                header('Content-Type:text/html; charset=utf-8');
                exit($data);
            default     :
                // 用于扩展其他返回格式数据

        }
    }
}