<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/9/4 0004
 * Time: 10:31
 */
namespace App\Red\Model;
use Phalcon\Mvc\MongoCollection;

/**
 * mong联接
 * Class LogTestModel
 * @package App\Red\Model
 */
class LogTestModel extends MongoCollection
{
    public $_id;
    public $uid;                  //appid
    public $openid;             //发放微信
    public $pay_date;        //支付日期
    public $pay_time;       //支付时间
    public $money;           //金额
    public $act_name;     //红包名称
    public $unified;
    public $unifiedOrder;  //内容
    public $pay_status;  // 装态值
    public function initialize()
    {
        $this->setConnectionService('mongo');
    }


}