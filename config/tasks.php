<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/8/10 0010
 * Time: 11:56
 */

return new \Phalcon\Config( [
    'default' => [ 'process_num' => 1,
    ],
    'swSpareTaskSize' => 2560,
    'swWorkingTasksSize' => 2560,
    'dispatcher_process_num_enable' => false,
    'tasks' => [
        'for1' => [
            'title' => 'for1',
            'enable' => true,
            'process_num' => 4,
            'class' => 'App\\Task\\For1Task',
        ],
        'for2' => [
            'title' => 'for2',
            'enable' => true,
            'process_num' => 4,
            'class' => 'App\\Task\\For2Task',
        ],
    ]
]);