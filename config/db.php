<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/8/9 0009
 * Time: 15:39
 */
return new \Phalcon\Config(array(
    /**
     * 数据库配置
     */
    'db' => array(
        'adapter'     => 'Mysql',
        "host"     => "127.0.0.1",
        "username" => "root",
        "password" => "mima_8080",
        "dbname"   => "phalcon",
        'charset'	  => 'utf8',
        'prefix' 	  => 'ks_'
    ),
));