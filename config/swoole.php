<?php
/**
 * swoole http server config
 * User: bruce
 * Date: 2018/7/15
 * Time: 上午10:48
 */

return new \Phalcon\Config(array(
    'worker_num' => 4,
    'daemonize' => false,
    'max_request' => 0,
    'task_worker_num' => 256,
    'host' => '127.0.0.1',
    'port' => 9501,
    'pid_file' => SERVER_ROOT . 'server.pid',
    'log_file'=>DOCUMENT_ROOT.'storage/logs/swoole.log'
));