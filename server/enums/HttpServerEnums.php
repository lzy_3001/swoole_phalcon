<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/8/9 0009
 * Time: 11:12
 */
namespace server\enums;
class HttpServerEnums
{
    /**
     * session key
     */
    const SESSION_KEY = 'SESSIONID';//'MOBILESESSIONID';

    /**
     * http server software: php-fpm libphp ...
     */
    const SERVER_TYPE_COMMON = 0;

    /**
     * http server software: swoole
     */
    const SERVER_TYPE_SWOOLE = 1;


    /**
     * session fetch mode get all
     */
    const SESSION_FETCH_MODE_ALL = 0;

    /**
     * session fetch mode get single
     */
    const SESSION_FETCH_MODE_SINGLE = 1;

    /**
     * swoole master task mgr
     */
    const MASTER_PROCESS_TITLE = '5v5_taskmgr';

}