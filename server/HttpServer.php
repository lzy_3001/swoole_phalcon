<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/8/8 0008
 * Time: 17:43
 */

namespace server;

use server\enums\HttpServerEnums;

/**
 * 启动服swoole 服务
 * Class HttpServer
 * @package server
 */
class HttpServer
{
    use \server\WorkerStart;
    use \server\Request;
    use \server\TasksMgrTrait;
    public static $instance;
    private $_http;
    private $_swooleCfg = null;
    private $_taskMgr = null;

    private $application;  //phalcon应用

    private function __construct($arrCfg = null)
    {
        /**
         * 初始化配置
         */
        $this->initSwooleHttpServer($arrCfg);
        /**
         * 初始化任务异步
         */
        $this->initTaskMgr();

        $this->_http->on('WorkerStart', array($this, 'onWorkerStart'));  //启动

        $this->_http->on('WorkerStop', array($this, 'onWorkerStop')); //关机

        $this->_http->on('Shutdown', array($this, 'onShutdown'));   //

        $this->_http->on("start", array($this, 'onStart'));

        $this->_http->on('request', array($this, 'onRequest'));

        $this->_http->on('task', array($this, 'onTask'));

        $this->_http->on('finish', array($this, 'onFinish'));

        $this->_http->start();

    }

    private function initSwooleHttpServer($arrCfg)
    {

        $this->_swooleCfg = include dirname(__DIR__) . '/config/swoole.php';

        $this->_http = new \swoole_http_server($this->_swooleCfg->host, $this->_swooleCfg->port);

        $this->_http->set(array_merge($this->_swooleCfg->toArray(), $arrCfg));
        return $this->_http;
    }

    private function initTaskMgr()
    {
        assert($this->_http);
        $this->_taskMgr = new \server\TaskMgr($this->_http);
    }




    public function onStart($server)
    {
        echo 'Starting...' . PHP_EOL;
        if (php_uname('s') != 'Darwin') {
            cli_set_process_title(HttpServerEnums::MASTER_PROCESS_TITLE);
        }
    }

    public function onTask($server, $task_id, $reactor_id, $params)
    {
         print_r($params);
        echo "New AsyncTask[id=$task_id]--task-" . PHP_EOL;
        //返回任务执行的结果
        $this->_http->finish("$params -> OK");
////        if (array_key_exists($arrParams->taskInfo->taskLabel, $this->_taskMgr->getArrValidTasks()))
////        {
////
////            $this->_taskMgr->changeWorkerStatus( $server, $arrParams->taskInfo );
////
////            $this->_taskMgr->doTaskWork( $server, $task_id, $reactor_id, $arrParams, $this->application->getDI() );
////
////            $server->finish(json_encode($arrParams->taskInfo));
////        }
////        else
////        {
////            echo 'worker not exist!', PHP_EOL;
////        }
    }

    public function onFinish($server, $task_id, $params)
    {
        echo 'finished', PHP_EOL;

        //$arrParams = json_decode($params);

        //$this->_taskMgr->resetTask($arrParams);
    }

    public function onShutdown($server)
    {
        if (isset($this->_swooleCfg->pid_file)) {
            unlink($this->_swooleCfg->pid_file);
        }
    }


    public function onWorkerStop($server, $worker_id)
    {
        echo 'worker: ', $worker_id, 'stopped', PHP_EOL;
    }

    public static function getInstance($arrCfg = null)
    {
        if (!self::$instance) {
            self::$instance = new HttpServer($arrCfg);
        }

        return self::$instance;
    }

}