<?php
/**
 * Product: rdc_analysis.
 * Date: 18-7-25
 * Time: 下午4:01
 */

namespace server;


trait TasksMgrTrait
{

    /**
     * @param $arrTasks
     *
     * @return mixed
     */
    public function getValidTasks( $arrTasks )
    {
        $arrValidTasks = $this->_taskMgr->getValidTasks( $arrTasks );

        return $arrValidTasks;
    }


    /**
     * @param $taskLabel
     * @param $request
     * @param $http
     *
     * @return mixed
     */
    public function dispatchTask( $taskLabel, $processNum )
    {
        return $this->_taskMgr->dispatchTask( $taskLabel, $processNum, $this->_http );
    }


    public function getWorkerStatusInfo()
    {
        return $this->_taskMgr->getWorkerStatusInfo();
    }




}