<?php
/**
 * Created by 荣耀电竞.
 * User: 林子彦 <278805354@qq.com>
 * Date: 2018/8/10 0010
 * Time: 15:13
 */
namespace server;
use App\Base\Helpers\ApiResponse;

trait Request
{
    use ApiResponse;
    public function onRequest($request, $response)
    {
        try{  //当响应有异常进行处理
            if (empty($request->server['path_info']))
            {
                $response->end('error request no path_info!<br>');
                return;
            }
            $strHtml = $this->application->handle($request->server['path_info'])->getContent();
            $response->gzip(1);
            $response->end($strHtml);
        }catch (\Exception $e){
            $response->end($this->error("错误信息:".$e->getMessage()));
        }

    }
}