# swoole_phalcon

#### 项目介绍
phalcon与swoole集成

#### 软件架构
软件架构说明


#### 安装教程

- 安装扩展包
```
    composer install
```
- 导入数据库表
```
  source bak.sql
```
- 修改配置文件
```
 config/config.php
```
```   
         "host"     => "127.0.0.1",
         "username" => "root",
         "password" => "mima_8080",
         "dbname"   => "phalcon",
         'charset'	  => 'utf8',
         'prefix' 	  => 'ks_'
```
- nginx 代理配置

```
   upstream www.phalcon.com {
          server  127.0.0.1:9501;
    }
   server{
        listen 80;
        server_name www.phalcon.com;
        location / {
            proxy_pass         http://www.phalcon.com;
            proxy_set_header   Host             $host;
            proxy_set_header   X-Real-IP        $remote_addr;
            proxy_set_header   X-Forwarded-For  $proxy_add_x_forwarded_for;
        }
    }
```
- 启动swoole 服务
```
cd /server
```
```
php server.php -d               run in daemonize mode
php server.php -r               restart the server
php server.php -D               run in foreground mode
php server.php --worker_num=N   set runing http worker
php server.php stop             stop the server
php server.php -h               show help  
```






#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)